if version > 580
  hi clear
  if exists("syntax_on")
    syntax reset
  endif
endif
let g:colors_name='meltdown'

lua require("meltdown").reload_package("meltdown")
lua require('meltdown.highlights').highlight(require('meltdown').colors)
