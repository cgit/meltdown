local colors = require('meltdown').colors
local vim = assert(vim)

local n_flip = colors.blue
local i_flip = colors.radioactive_green
local v_flip = colors.gold_2
local r_flip = colors.red
local c_flip = colors.stormy_blue
local t_flip = colors.lighter_grey

return {
  normal = {
    a = {bg = n_flip, fg = colors.midnight, gui = 'bold'},
    b = {bg = colors.midnight, fg = n_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  },
  insert = {
    a = {bg = i_flip, fg = colors.black, gui = 'bold'},
    b = {bg = colors.midnight, fg = i_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  },
  visual = {
    a = {bg = v_flip, fg = colors.midnight, gui = 'bold'},
    b = {bg = colors.midnight, fg = v_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  },
  replace = {
    a = {bg = r_flip, fg = colors.midnight, gui = 'bold'},
    b = {bg = colors.midnight, fg = r_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  },
  command = {
    a = {bg = c_flip, fg = colors.midnight, gui = 'bold'},
    b = {bg = colors.midnight, fg = c_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  },
  terminal = {
    a = {bg = t_flip, fg = colors.midnight, gui = 'bold'},
    b = {bg = colors.midnight, fg = t_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  },
  inactive = {
    a = {bg = n_flip, fg = colors.midnight, gui = 'bold'},
    b = {bg = colors.midnight, fg = n_flip},
    c = {bg = colors.black, fg = colors.light_yellow, bold = true}
  }
}
