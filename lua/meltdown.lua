local vim = assert(vim)

local meltdown = {}

function meltdown.remove_package(str)
  for k, _ in pairs(package.loaded) do
    if string.match(k, "^" .. str) then
      package.loaded[k] = nil
    end
  end
end

function meltdown.reload_package(str)
  meltdown.remove_package(str)
  return require(str)
end

meltdown.colors = {
  bg_0 = "#16161D",
  bg_1 = "#181820",
  bg_2 = "#1a1a22",
  bg_3 = "#1f1f28",
  bg_4 = "#2a2a37",
  bg_5 = "#363646",
  bg_6 = "#54546d",

  near_white = '#e0e0e0',
  normal_fg = '#d8e5b8',

  blue = '#80a0ff',
  dark_grey = '#404040',
  midnight = '#003040',
  gold_1 = "#ffa034",
  gold_2 = '#ffb700',
  gold_5 = "#ffbd2f",
  gold_3 = "#ffb75f" ,
  gold_4 = "#ffd780",
  light_yellow = "#fce094",
  salmon = '#ff7779',
  salmon_2 = "#ff9074",
  lighter_grey = '#9a9a9a',
  red_2 = "#ff552f",
  red = '#ff5d34',
  all_red = '#ff0000',
  half_red = '#790000',
  pink = '#ffc0b9',
  sage = '#9f9a84',
  sage_2 = '#b0c4a3',
  teal = '#76dbd8',
  teal_2 = '#8cf8f7',
  teal_3 = '#71fffa',
  stormy_blue = '#8ba3bb',
  dark_cloud = '#4b4b64',
  magenta = '#ff74f4',
  orange = '#ff8f34',
  cobalt = '#00d0ff',
  radioactive_green = '#26ff00',
  black = '#000000',
  olive = "#aba024",
  purple = "#af80ff",
}

return meltdown
